import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { getLocaleDateFormat } from '@angular/common';


@Component({
  selector: 'app-country-card',
  templateUrl: './country-card.component.html',
  styleUrls: ['./country-card.component.css']
})


export class CountryCardComponent implements OnInit {
  url = 'https://restcountries.eu/rest/v2/all';
  items = [];

  localDate = new Date();
  utcDate = this.localDate.getUTCDate();
  utcMonth = this.localDate.getUTCMonth();
  utcYear = this.localDate.getUTCFullYear();
  utcHour = this.localDate.getUTCHours();
  utcMin = this.localDate.getUTCMinutes();
  utcSec = this.localDate.getUTCSeconds();

  //utc = new Date(this.utcYear, this.utcMonth, this.utcDate, this.utcHour, this.utcMin, this.utcSec, 0);
  

  constructor(private http: HttpClient) {
    this.http.get(this.url, { responseType: 'json'}).toPromise().then(data => {
      console.log(data);

      for (let key in data){
        if (data.hasOwnProperty(key)){
          this.items.push(data[key])
        }
      }

      for (let item in this.items) {
        

        this.items[item].timezones[0].search(":") ? 1 : this.items[item].timezones[0] = (this.items[item].timezones[0] + ":00");
        this.items[item].timezones[0] == "UTC" ? this.items[item].timezones[0] = "+00:00" : 1;
        

        let timezone = this.items[item].timezones[0].split('C')[1];
        let hours = parseInt(timezone.split(':')[0]);
        let mins = parseInt(timezone.split(':')[1]);
        
        let local = this.localDate;  
        local.setHours(local.getHours() + (hours))
        local.setMinutes(local.getHours() + (mins))

        this.items[item].time = local; 
        

      }
      // for (let item in this.items) {
      //   console.log(this.items[item].timezones[0]);

      // }

      
    })
  }

  ngOnInit(): void {
  }

}
